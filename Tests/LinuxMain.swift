import XCTest

import PersianDatePickerPackageTests

var tests = [XCTestCaseEntry]()
tests += PersianDatePickerPackageTests.allTests()
XCTMain(tests)
